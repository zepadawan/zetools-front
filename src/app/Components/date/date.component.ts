import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss']
})
export class DateComponent implements OnInit {

  private date = new Date();
  public day = this.date.getDay();
  public month = this.date.getMonth();

  constructor() { }

  ngOnInit(): void {
    setInterval(() => {
      this.date = new Date();
    }, 30000);
  }

  getDay(): string {
    switch (this.day) {
      case 0: {
        return 'dimanche';
      }
      case 1: {
        return 'lundi';
      }
      case 2: {
        return 'mardi';
      }
      case 3: {
        return 'mercredi';
      }
      case 4: {
        return 'jeudi';
      }
      case 5: {
        return 'vendredi';
      }
      case 6: {
        return 'samedi';
      }
      default: {
        return 'jour inconnu';
      }
    }
  }

  getMonth(): string {
    switch (this.month) {
      case 0: {
        return 'janvier';
      }
      case 1: {
        return 'février';
      }
      case 2: {
        return 'mars';
      }
      case 3: {
        return 'avril';
      }
      case 4: {
        return 'mai';
      }
      case 5: {
        return 'juin';
      }
      case 6: {
        return 'juillet';
      }
      case 7: {
        return 'août';
      }
      case 8: {
        return 'septembre';
      }
      case 9: {
        return 'octobre';
      }
      case 10: {
        return 'novembre';
      }
      case 11: {
        return 'décembre';
      }
      default: {
        return 'mois inconnu';
      }
    }
  }

  getDate(): string {
    return String(this.date.getDate());
  }

  getHours(): string {
    let hour = String(this.date.getHours());
    let minute = String(this.date.getMinutes());
    if (this.date.getHours() <= 9) {
      hour = `0${this.date.getHours()}`;
    }

    if (this.date.getMinutes() <= 9) {
      minute = `0${this.date.getMinutes()}`;
    }

    return `${hour}:${minute}`;
  }

  getYear(): string {
    return String(this.date.getFullYear());
  }

  getCustomDateFormat(): string {
    return `${this.getDay()}, ${this.getDate()} ${this.getMonth()} ${this.getYear()} ${this.getHours()}`;
  }

}
